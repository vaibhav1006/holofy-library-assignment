const express = require('express');
const router = express.Router();
const userController = require('../controllers/user')

router.post('/registrationUsingEmail', userController.registrationUsingEmail)

router.post('/loginUsingEmail', userController.loginUsingEmail)

module.exports = router;