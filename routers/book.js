const express = require('express');
const router = express.Router();
const bookController = require('../controllers/book')
const userAuth = require('../middleware/userAuth')
const adminAuth = require('../middleware/adminAuth')
router.post('/', userAuth, bookController.addBook)

router.patch('/:id', userAuth, bookController.updateBookDetails)
router.delete('/:id', userAuth, bookController.deleteBook)

router.get('/', userAuth, bookController.getBooks)

router.get('/:id', userAuth, bookController.getBookDetails)

router.delete('/', adminAuth, bookController.softDelete)


module.exports = router;