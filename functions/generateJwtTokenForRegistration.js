
const jwt = require('jsonwebtoken');

//generate user jwt token
function gerenateJwtTokenForUser(userData) {

  return jwt.sign(userData, process.env.REGISTRATION_JWT_TOKEN_SECRET_KEY, { expiresIn: '365d'});// this expires in 365 days

}

function convertStringToDate(stringDate) {
  resultDate = new Date();
  stringDate = stringDate.split("/");
  resultDate.setDate(Number(stringDate[0]));
  resultDate.setMonth(Number(stringDate[1]));
  resultDate.setFullYear(Number(stringDate[2]));
  return resultDate
}


module.exports.gerenateJwtTokenForUser = gerenateJwtTokenForUser;
module.exports.convertStringToDate = convertStringToDate;
