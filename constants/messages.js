module.exports = {
    failedToMatchMobileNumberPattern: 'mobile number: all characters must be interges',
    alreadyRegistered: 'Already Registered',
    registerFirst: 'You are not registered yet!',
    registeredSuccessfully: 'Successfully registered',
    loginSuccessfully: 'Successfully login',
    somethingWentWrong: 'Oops! Something went wrong here...',
    tokenError: 'You need to provide token for authentication.',
    userNotFound: 'The user does not exists on this platform.',
    tokenExpired: 'This token has been expired.',
    invalidMovileNumerOrPasswors: 'The email or password you have entered is invalid.',
    bookAlreayFound: "Book already found",
    newBookAdded: "Successfully added new book",
    bookNotFound: "book is not found with the given id",
    updatedBookDetail: "Successfully updated book details",
    releaseDateError: 'release date should be future date',
    deletedBook: 'Successfully deleted book',
    getAllBooks: 'Successfully get all books',
    successfullyGetBookDetail: 'Successfully get book details',
    successfullySoftDeletedAllBooks: 'Successfully soft deleted all books'
}