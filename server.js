const express = require('express');

const app = express();

const cors = require('cors');

const port = process.env.PORT || 8000;

app.use('*',cors());



app.use(express.json()); // support json encoded bodies
app.use(express.urlencoded({ extended: true })); // support encoded bodies


// config any key is provided in .env file
require('./startup/config')()
// connection to mongodb
require('./startup/db')()
require('./startup/routes')(app);



app.listen(port, () => {
    console.log(`Server is running on port ${port}`)
})
