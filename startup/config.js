'use strict';

require('dotenv').config()

module.exports = function (){
   
    // if mongodb uri is not provided or found 
    if(!process.env.MONGODB_URI){
        throw new Error('FATEL ERROR : mongodb url not defined..');    
    }
    if(!process.env.REGISTRATION_JWT_TOKEN_SECRET_KEY){
        throw new Error('FATEL ERROR : REGISTRATION_JWT_TOKEN_SECRET_KEY not defined..');    
    }
    if(!process.env.GATE_VALUE){
        throw new Error('FATEL ERROR : GATE_VALUE not defined..');    
    }
}
