const mongoose = require('mongoose');

module.exports = async function(){   

   const url = process.env.MONGODB_URI   
   const connectionParams = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    };
    try{
               
        await mongoose.connect(url,connectionParams)
        console.log('connected to the mongodb')
    }catch(e){
        console.log('error ','mongodb connection error...', e);
    }
}