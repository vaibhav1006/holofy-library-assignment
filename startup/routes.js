const user = require('../routers/user')
const book = require('../routers/book')
const admin = require('../routers/admin')
module.exports = function (app) {
    
    
    
    app.use('/user', user)
    app.use('/books', book)
    app.use('/admin', admin)
}