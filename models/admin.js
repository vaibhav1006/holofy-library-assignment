const Joi = require("joi");
const mongoose = require("mongoose");


const Schema = mongoose.Schema;

const adminSchema = new Schema(
  {
    email: {
      // mobile number of user
      type: String,
      required: true,
    },
   password: {
     // password of user
    type: String,
    required: true
   },
    /* uniq tag of user 
    if user logged in new device then another token should expire for that user tag */
    etag: {
      type: String,
      default: false,
    },
  },
  { timestamps: true }
);


const Admin = mongoose.model("admins", adminSchema);


function validateLoginBody(body){
  const schema = Joi.object().keys({
    // email should be valid email
    email: Joi.email().required(),
    password: Joi.string().min(5).required()
  });
  return schema.validate(body, { abortEarly: false });
}

module.exports.Admin = Admin;

module.exports.validateLoginBody = validateLoginBody;
