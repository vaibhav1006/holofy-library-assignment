const Joi = require("joi");
const mongoose = require("mongoose");


const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    email: {
      // mobile number of user
      type: String,
      required: true,
    },
   password: {
     // password of user
    type: String,
    required: true
   },
    /* uniq tag of user 
    if user logged in new device then another token should expire for that user tag */
    etag: {
      type: String,
      default: false,
    },
  },
  { timestamps: true }
);


const Users = mongoose.model("users", userSchema);

function validateRegistrationUsingEmailBody(body) {
  const schema = Joi.object().keys({
    
    // email should be valid email
    email: Joi.string().email().required(),
    password: Joi.string().min(5).required()
  });
  return schema.validate(body, { abortEarly: false });
}


function validateLoginBody(body){
  const schema = Joi.object().keys({
    // email should be valid email
    email: Joi.string().email().required(),
    password: Joi.string().min(5).required()
  });
  return schema.validate(body, { abortEarly: false });
}

module.exports.Users = Users;
module.exports.validateRegistrationUsingEmailBody = validateRegistrationUsingEmailBody;

module.exports.validateLoginBody = validateLoginBody;
