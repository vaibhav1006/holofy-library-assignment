const Joi = require("joi").extend(require("@joi/date"));
Joi.objectId = require('joi-objectid')(Joi);


const mongoose = require("mongoose");
const Messages = require("../constants/messages");

const Schema = mongoose.Schema;

const bookSchema = new Schema(
  {
    name: {
      // name of book
      type: String,
      lowercase: true,
      required: true,
    },
    description: {
      // description of book
      type: String,
      lowercase: true,
      required: true,
    },
    author: {
      // author of  book
      type: String,
      lowercase: true,
      required: true
    },
    isDisabledByAdmin: {
      // this is for soft delete flag
      type: Boolean,
      required: true,
      default: false
    },
    releaseDate: {
      type: Date,
      required: true
    }
  },
  { timestamps: true }
);

const Books = mongoose.model("books", bookSchema);

function validateAddBookBody(body) {
  const schema = Joi.object().keys({
    name: Joi.string().min(3).required(), // name of book must be minimum 3 character
    description: Joi.string().min(3).required(), // description of book must be minimum 3 character
    author: Joi.string().min(3).required(),
    releaseDate: Joi.date() // release date should in "DD/MM/YYY" format
    .format("DD/MM/YYYY")
    .required(),
  });
  return schema.validate(body, { abortEarly: false });
}

function validateUpdateBookDatilsBody(body) {
  const schema = Joi.object().keys({
    name: Joi.string().lowercase({force: true}).min(3).required(), // name of book must be minimum 3 character
    description: Joi.string().lowercase({force: true}).min(3).required(), // description of book must be minimum 3 character
    author: Joi.string().min(3).lowercase({force: true}).required(),
    releaseDate: Joi.date() // release date should in "DD/MM/YYY" format
    .format("DD/MM/YYYY")
    .required(),
  });
  return schema.validate(body, { abortEarly: false });
}

function validateUpdateBookDetailsParams(body){
  const schema = Joi.object({
    id: Joi.objectId().required(),
  });
  return schema.validate(body);
}

function validategetBooksQuery(body){
  const schema = Joi.object({
    page: Joi.number().min(1).required(),
    limit: Joi.number().min(1).required()
  });
  return schema.validate(body);
}

module.exports.Books = Books;
module.exports.validateAddBookBody = validateAddBookBody;
module.exports.validateUpdateBookDetailsParams = validateUpdateBookDetailsParams;
module.exports.validateUpdateBookDatilsBody = validateUpdateBookDatilsBody;
module.exports.validategetBooksQuery = validategetBooksQuery;
