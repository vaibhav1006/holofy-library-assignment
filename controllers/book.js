
  
  const Messages = require("../constants/messages");
  const {Books, validateAddBookBody, validateUpdateBookDetailsParams, validateUpdateBookDatilsBody, validategetBooksQuery } = require('../models/book')
  const moment = require('moment')

const _ = require('lodash')
  /*
  add book 
  */
  
  const addBook = async (req, res) => {
    try {
      // validate body
    const { error } = validateAddBookBody(req.body);
    // if client send invalid data return 400 'Invalid Request'
    if (error) {
      return res.status(400).send({
        statusCode: 400,
        error: "Invalid Request",
        message: error.message,
      });
    }
    let isBook = await Books.findOne({$and: [{ name : { '$regex' : req.body.name, '$options' : 'i' }}, { author : { '$regex' : req.body.author, '$options' : 'i' }}]});
    if(isBook){
      return res.status(400).send({ statusCode : 400, error : 'Invalid Request' , message : Messages.bookAlreayFound })
    }
      req.body.releaseDate = new Date(moment(req.body.releaseDate, 'DD/MM/YYYY'))
      req.body.releaseDate.setDate(req.body.releaseDate.getDate() + 1)
      newBook = await new Books(req.body).save()
      return res.status(200).send({
        statusCode: 200,
        data: newBook,
        message: Messages.newBookAdded,
      });
    } catch (e) {
      // send 500 error if something goes wrong
      console.log(e);
      return res.status(500).send({
        statusCode: 500,
        message: Messages.somethingWentWrong,
        error: e.message,
      });
    }
  };
  
  
  /*
  update book details
  */
  
  const updateBookDetails = async (req, res) => {
    try {
    
    var { error } = validateUpdateBookDetailsParams(req.params);
    if (error) {
      return res.status(400).send({
        statusCode: 400,
        error: "Invalid Request",
        message: error.message,
      });
    }
    // validate body
    var { error } = validateUpdateBookDatilsBody(req.body);
    // if client send invalid data return 400 'Invalid Request'
    if (error) {
      return res.status(400).send({
        statusCode: 400,
        error: "Invalid Request",
        message: error.message,
      });
    }
    let isBook = await Books.findById(req.params.id);
    if(!isBook){
      return res.status(400).send({ statusCode : 400, error : 'Invalid Request' , message : Messages.bookNotFound })
    }
      isFieldChanged = 0
      if(req.body.releaseDate){
      req.body.releaseDate = new Date(moment(req.body.releaseDate, 'DD/MM/YYYY'))
      req.body.releaseDate.setDate(req.body.releaseDate.getDate() + 1)
      // check any change in date 
      if(isBook.releaseDate.getTime() !== req.body.releaseDate.getTime()){
        isFieldChanged = 1
        console.log(isBook.releaseDate)
        isBook.releaseDate = req.body.releaseDate
        console.log('in release',  req.body.releaseDate)
      }
      req.body = _.omit(req.body, ['releaseDate'])
      }
      // check any change in data
      for(key of Object.keys(req.body)){
        if(isBook[key] !== req.body[key]){
          console.log('in key')
          isFieldChanged = 1
          isBook[key] = req.body[key]
        }
      }
      if(isFieldChanged){
        console.log('hey')
      await isBook.save()
      }
      return res.status(200).send({
        statusCode: 200,
        data: isBook,
        message: Messages.newBookAdded,
      });
    } catch (e) {
      // send 500 error if something goes wrong
      console.log(e);
      return res.status(500).send({
        statusCode: 500,
        message: Messages.somethingWentWrong,
        error: e.message,
      });
    }
  };
  
  
  /**
   * delete book
   */

  const deleteBook = async (req, res) => {
    try {
    // validate params
    var { error } = validateUpdateBookDetailsParams(req.params);
    if (error) {
      return res.status(400).send({
        statusCode: 400,
        error: "Invalid Request",
        message: error.message,
      });
    }
    let isBook = await Books.deleteOne({_id: req.params.id});
    console.log(isBook)
    if(isBook.deletedCount === 0){
      return res.status(400).send({ statusCode : 400, error : 'Invalid Request' , message : Messages.bookNotFound })
    }
      
      return res.status(200).send({
        statusCode: 200,
        data: isBook,
        message: Messages.deletedBook,
      });
    } catch (e) {
      // send 500 error if something goes wrong
      console.log(e);
      return res.status(500).send({
        statusCode: 500,
        message: Messages.somethingWentWrong,
        error: e.message,
      });
    }
  };


 /**
   * get books
   */

  const getBooks = async (req, res) => {
    try {
    // validate params
    var { error } = validategetBooksQuery(req.query);
    if (error) {
      return res.status(400).send({
        statusCode: 400,
        error: "Invalid Request",
        message: error.message,
      });
    }
    // get total documets
    totalCounts = await Books.countDocuments({isDisabledByAdmin: false});
    if(totalCounts === 0){
      return res.status(200).send({statusCode: 200,
        data: [],
        currentPage: req.query.page,
        totalPages: 0,
        message: Messages.getAllBooks
      })
    }
    // get all books
    allBooks = await Books.find({isDisabledByAdmin: false}, '-description')
      .sort({releaseDate: -1})
      .skip(req.query.limit * (req.query.page - 1))
      .limit(Number(req.query.limit))
    
      
      return res.status(200).send({
        statusCode: 200,
        currentPage: req.query.page,
      totalPages: Math.ceil(totalCounts / req.query.limit),
        data: allBooks,
        message: Messages.getAllBooks,
      });
    } catch (e) {
      // send 500 error if something goes wrong
      console.log(e);
      return res.status(500).send({
        statusCode: 500,
        message: Messages.somethingWentWrong,
        error: e.message,
      });
    }
  };


 /*
  get book details
  */
  
  const getBookDetails = async (req, res) => {
    try {
    
    var { error } = validateUpdateBookDetailsParams(req.params);
    if (error) {
      return res.status(400).send({
        statusCode: 400,
        error: "Invalid Request",
        message: error.message,
      });
    }

    let isBook = await Books.findById(req.params.id, 'description');
    if(!isBook){
      return res.status(400).send({ statusCode : 400, error : 'Invalid Request' , message : Messages.bookNotFound })
    }
      
      return res.status(200).send({
        statusCode: 200,
        data: isBook,
        message: Messages.successfullyGetBookDetail,
      });
    } catch (e) {
      // send 500 error if something goes wrong
      console.log(e);
      return res.status(500).send({
        statusCode: 500,
        message: Messages.somethingWentWrong,
        error: e.message,
      });
    }
  };


  /*
  soft delete for admin
  */
  
  const softDelete = async (req, res) => {
    try {
    
    
    let isBook = await Books.updateMany({_id: {$ne: null}}, {isDisabledByAdmin: true});
    
      
      return res.status(200).send({
        statusCode: 200,
        data: isBook,
        message: Messages.successfullySoftDeletedAllBooks,
      });
    } catch (e) {
      // send 500 error if something goes wrong
      console.log(e);
      return res.status(500).send({
        statusCode: 500,
        message: Messages.somethingWentWrong,
        error: e.message,
      });
    }
  };
  module.exports = {
    addBook,
    updateBookDetails,
    deleteBook,
    getBooks,
    getBookDetails,
    softDelete
    
  };
  