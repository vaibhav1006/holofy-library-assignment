const {
  Users,
  validateRegistrationUsingEmailBody,
  validateLoginBody
} = require("../models/users");
const Messages = require("../constants/messages");
const {
  gerenateJwtTokenForUser,
} = require("../functions/generateJwtTokenForRegistration");
const bcrypt = require("bcryptjs");



/*
registration of user
*/

const registrationUsingEmail = async (req, res) => {
  try {
    // validate body
    const { error } = validateRegistrationUsingEmailBody(req.body);
    // if client send invalid data return 400 'Invalid Request'
    if (error) {
      return res.status(400).send({
        statusCode: 400,
        error: "Invalid Request",
        message: error.message,
      });
    }
    isUser = await Users.findOne({ email: req.body.email });
    if (isUser) {
      return res
        .status(400)
        .send({ statusCode: 400, message: Messages.alreadyRegistered });
    }

    newUser = new Users({
      email: req.body.email,
    });
    newUser.password = req.body.password;
    // create salt for password
    const salt = await bcrypt.genSalt(10);
    // update password with hashed
    newUser.password = bcrypt.hashSync(newUser.password, salt);
    // update user information from the database

    const token = gerenateJwtTokenForUser({ id: newUser._id });
    // convert userid to string
    const uId = new String(newUser._id).toString();
    // generate unique pattern
    const x =
      token.substr(token.length - 43, token.length) +
      uId.substr(uId.length - 15, uId.length - 1) +
      process.env.GATE_VALUE;
    // set user etag
    newUser.etag = bcrypt.hashSync(x, salt);
    newUser = await newUser.save();
    return res.status(200).send({
      statusCode: 200,
      token: token,
      message: Messages.registeredSuccessfully,
    });
  } catch (e) {
    // send 500 error if something goes wrong
    console.log(e);
    return res.status(500).send({
      statusCode: 500,
      message: "Oops! Something went wrong here...",
      error: e.message,
    });
  }
};





/*
login using email
*/

const loginUsingEmail = async (req, res) => {
  try {
    // validate body
    const { error } = validateLoginBody(req.body);
    // if client send invalid data return 400 'Invalid Request'
    if (error) {
      return res.status(400).send({
        statusCode: 400,
        error: "Invalid Request",
        message: error.message,
      });
    }
    
    isUser = await Users.findOne({ email: req.body.email });
    if (!isUser) {
      return res
        .status(400)
        .send({ statusCode: 400, message: Messages.registerFirst });
    }
    const validPassword = await bcrypt.compare(req.body.password, isUser.password);
    // if password is not valid return with 400 error 
    if(!validPassword) return res.status(400).send({ statusCode : 400 , message : Messages.invalidMovileNumerOrPasswors });
    const token = gerenateJwtTokenForUser({id: isUser._id}); 
    // convert userid to string
        const uId = new String(isUser._id).toString();
        // generate unique pattern
        const x = token.substr(token.length-43, token.length) + uId.substr(uId.length-15, uId.length-1 ) + process.env.GATE_VALUE;
        // create new salt value
        const salt = await bcrypt.genSalt(10);
        // set user etag 
        isUser.etag = bcrypt.hashSync(x, salt);
        isUser = await isUser.save(); 
    
    return res.status(200).send({
      statusCode: 200,
      token: token,
      message: Messages.loginSuccessfully,
    });
  } catch (e) {
    // send 500 error if something goes wrong
    console.log(e);
    return res.status(500).send({
      statusCode: 500,
      message: Messages.somethingWentWrong,
      error: e.message,
    });
  }
};



module.exports = {
  registrationUsingEmail,
  loginUsingEmail
};
