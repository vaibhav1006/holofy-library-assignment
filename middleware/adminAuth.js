const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { Admin } = require("../models/admin");
const Messages = require('../constants/messages')
module.exports = async function (req, res, next) {
  try {
    // get the token from req
    const token = req.header("x-auth-token");
    // if token not provided return error
    if (!token)
      return res
        .status(401)
        .send({
          statusCode: 401,
          error: "Unauthorized.",
          message: Messages.tokenError,
        });
    // validate token and get hash of data
    const decodedDate = jwt.verify(
      token,
      process.env.REGISTRATION_JWT_TOKEN_SECRET_KEY
    );
  

    // get the user by decoded id
    const user = await Admin.findById(
      decodedDate.id,
    );
    // if user not found return error
    if (!user)
      return res
        .status(401)
        .send({
          statusCode: 401,
          error: "Unauthorized.",
          message: Messages.userNotFound,
        });

    // convert userId to string
    const uId = user.id;

    // create combination string to modify
    const x = token.substr(token.length-43, token.length) + uId.substr(uId.length-15, uId.length-1 ) + process.env.GATE_VALUE;
    

    // validate token
    const validateToken = await bcrypt.compare(x, user.etag);
    

    // if invalid token return 410 'Unauthorised'
    if (!validateToken)
      return res
        .status(410)
        .send({
          statusCode: 410,
          error: "Unauthorized",
          message: Messages.tokenExpired,
        });


    //set req.user
    req.user = {
      id: user.id,
    };

    next();
  } catch (ex) {
    console.log(ex);
    // send error if something goes wrong
    return res
      .status(410)
      .send({
        statusCode: 410,
        error: "Unauthorized",
        message: Messages.tokenExpired,
      });
  }
};
