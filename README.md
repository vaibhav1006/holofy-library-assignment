
# Project Title

A brief description of what this project does and who it's for


# holofy library assignment

## postman collection link
https://www.getpostman.com/collections/0f7e36d2ff9a1a73e3ff

import this collection


## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`MONGODB_URI`

`REGISTRATION_JWT_TOKEN_SECRET_KEY`

`GATE_VALUE`



### i have already deploy both assignments on heroku
#### for registration
go to user folder

For registration api Name  :- registrationUsingEmail heroku

body of api 

{
    "email": "mulajev@gmail.com",
    "password": "rajuma@1006"
}

#### for login
go to user folder
To login api Name  :- loginUsingemail heroku

body of api 

{
    "email": "mulajev@gmail.com",
    "password": "rajuma@1006"
}

#### for book apis
go book folder

add book 
api name : add book heroku
body of api
{
    "name": "how to win friends & people",
    "description": "This grandfather of all people-skills books was first published in 1937. It was an overnight hit, eventually selling 15 million copies. How to Win Friends and Influence People is just as useful today as it was when it was first published, because Dale Carnegie had an understanding of human nature that will never be outdated. Financial success, Carnegie believed, is due 15 percent to professional knowledge and 85 percent to ",
    "author": "Dale Carnegie",
    "releaseDate": "20/01/1936"
}

get all books

api name : get all books heroku

params page and limit both must be greater than 0


update book detail

api name : update book detail heroku

body: {
    "name": "how to win friends & infuence",
    "description": "This grandfather of all people-skills books was first published in 1937. It was an overnight hit, eventually selling 15 million copies. How to Win Friends and Influence People is just as useful today as it was when it was first published, because Dale Carnegie had an understanding of human nature that will never be outdated. Financial success, Carnegie believed, is due 15 percent to professional knowledge and 85 percent to ",
    "author": "Dale Carnegie",
    "releaseDate": "23/01/1936"
}

get book details

api name get book detail geroku

send id of book

delete book 
api name delete book heroku

soft delete books
this only for admin 
api name soft detail books heroku

admin credentials: {
    "email": "admin@gmail.com",
    "password": "password@123"
}



#### for admin
go to admin folder

To login  :- admin login heroku
I stored one login you use that 
{
    "email": "admin@gmail.com",
    "password": "password@123"
}


NOTE :- sometime application won't work on heroku because i used free trial of heroku. because of free trial heroku down the server

Don't worry i work on locally

# run application locally

## Installation

install node js and npm on your machine

for windows follow steps from this website: https://www.fosstechnix.com/how-to-install-node-js-on-windows/

for linux follow https://www.tecmint.com/install-nodejs-npm-in-centos-ubuntu/

for mac follow https://blog.teamtreehouse.com/install-node-js-npm-mac

## how to run application

go to holofy-library-assignment directory by

cd holofy-library-assignment

now run 

npm i

it will take some time to install packages

after that run

node server.js


now go to post imported postman collection 



##### for registration
go to user folder

For registration api Name  :- registrationUsingEmail 

body of api 

{
    "email": "mulajev@gmail.com",
    "password": "rajuma@1006"
}

#### for login
go to user folder
To login api Name  :- loginUsingemail 

body of api 

{
    "email": "mulajev@gmail.com",
    "password": "rajuma@1006"
}

#### for book apis
go book folder

add book 
api name : add book 
body of api
{
    "name": "how to win friends & people",
    "description": "This grandfather of all people-skills books was first published in 1937. It was an overnight hit, eventually selling 15 million copies. How to Win Friends and Influence People is just as useful today as it was when it was first published, because Dale Carnegie had an understanding of human nature that will never be outdated. Financial success, Carnegie believed, is due 15 percent to professional knowledge and 85 percent to ",
    "author": "Dale Carnegie",
    "releaseDate": "20/01/1936"
}

get all books

api name : get all books 

params page and limit both must be greater than 0


update book detail

api name : update book detail 

body: {
    "name": "how to win friends & infuence",
    "description": "This grandfather of all people-skills books was first published in 1937. It was an overnight hit, eventually selling 15 million copies. How to Win Friends and Influence People is just as useful today as it was when it was first published, because Dale Carnegie had an understanding of human nature that will never be outdated. Financial success, Carnegie believed, is due 15 percent to professional knowledge and 85 percent to ",
    "author": "Dale Carnegie",
    "releaseDate": "23/01/1936"
}

get book details

api name get book detail 

send id of book

delete book 
api name delete book 

soft delete books
this only for admin 
api name soft detail books 

admin credentials: {
    "email": "admin@gmail.com",
    "password": "password@123"
}



#### for admin
go to admin folder

yo can add one admin login
